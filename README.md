# MySite Template - README #
---

### Overview ###

**MySite** is a static, one page, HTML5 template which can be used and customized as a personal or portfolio site. The template is made only with plain HTML/CSS/JS and some [**Font Awesome**](https://fontawesome.com/) icons included in a small [**Fontello**](https://fontello.com/) font. No jQuery, no Bootstrap, no heavy Google fonts are used. Therefore the template is small and fast to load. The used example images are from [**Albumarium**](https://albumarium.com/) and are licensed under [**CC0 - No Rights Reserved**](https://creativecommons.org/share-your-work/public-domain/cc0/). For the sake of completeness, a Google map is embedded. But if you really want a fast and slim template, just remove this map from the code and get rid of that heavy bloat...

### Screenshots ###

![MySite - Home area](development/readme/mysite_1.png "MySite - Home area")
![MySite - About area](development/readme/mysite_2.png "MySite - About area")
![MySite - Work area](development/readme/mysite_3.png "MySite - Work area")
![MySite - Portfolio area](development/readme/mysite_4.png "MySite - Portfolio area")
![MySite - Contact area](development/readme/mysite_5.png "MySite - Contact area")

### Template Versions ###

* **index.html** - Minimized template with Google map.
* **index_no_gmap.html** - Minimized template without Google map.

* **index_plain.html** - Full template with all comments and Google map (not minimized).
* **index_plain_no_gmap.html** - Full template with all comments without Google map (not minimized).

### Setup ###

* Copy the directory **mysite** to your webroot directory.
* Make a copy of the file **index_plain.html** (or **index_plain_no_gmap.html**) and save it as **index.html**.
* Open the file **index.html** in your favorite text editor.
* Edit all sections of the template and insert your data.
* Customize also the social media links at the bottom of the page.
* Replace the images in the directory **mysite/images** with your images.
* Check the page in your browser and make modifications if necessary.
* Optional: Remove Google maps from your page if you want a fast and slim page!

### Support ###

This is a free template and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **MySite** template is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](http://opensource.org/licenses/MIT).
